#include <iostream>
#include <cstdlib>
#include <cstdio>
#include <algorithm>

using namespace std;

const int M=10000;
int x,n,i;
int s[M];

int BinarySearch(int s[],int x,int low,int high)
{
    if(low>high) return -1;
    int middle=(low+high)/2;
    if(x==s[middle])
        return middle;
    else if(x<s[middle])
    return BinarySearch(s,x,low,middle-1);
        else
         return BinarySearch(s,x,middle+1,high);

}

int main()
{
    cout<<"请输入数列中的元素个数n为 ： "<<endl;
    cin>>n;
    for(int i=0;i<n;i++)
        cin>>s[i];

    sort(s,s+n);
    cout<<"排序后的数组是："<<endl;

    for(int i=0;i<n;i++)
        cout<<s[i]<<" ";
    cout<<endl;
    cout<<"请输入要查找的元素x: "<<endl;
    cin>>x;
    i=BinarySearch(s,x,0,n-1);
    if(i==-1)cout<<"该数列中没有你要查找的元素。"<<endl;
    else cout<<x<<"在该数列的第"<<i+1<<"位上。"<<endl;

    return 0;

}
