#include <iostream>
#include <algorithm>
using namespace std;
const int N=100005;
double w[N];    //古董的重量数组
int main()
{
    double c;
    int n;
    cout<<"请输入载重量c及古董个数n："<<endl;
    cin>>c>>n;
    for(int i=0;i<n;i++)
        cin>>w[i];
    sort(w,w+n);
    double temp=0.0;   //已装载上船的重量
    int ans=0;         //已装载上船的古董数量
    for(int i=0;i<n;i++)      //★减少ans自增的时间，以达到节约运行时间的目的
      if(temp<c)
      {     temp+=w[i];
        if(temp<=c)
            {
                if(temp=c) ans=i+1;
                else ans=i;
            }

    }
    cout<<"能装载上船的最大数量为"<<ans<<"个."<<endl;

    return 0;
}
