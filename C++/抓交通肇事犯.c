#include <stdio.h>

//分析可知，前两位数字不可能为0，所以，不用考虑两位数的平方情况，只需要从4位数平方开始，因为30的平方小于四位数，所以从31开始平方
int main()
{
    int i,j,x,flag=0; //用flag来减少循环的次数，达到节约运行时间的目的
    for(i=0;i<10;i++)
    {
        if(flag)break;
        for(j=0;j<10;j++)
       {
           if(flag)break;

            if(i!=j)
           {
                for(x=31;x<=99;x++)
                    if(1000*i+100*i+11*j==x*x)
                    {
                        printf("车牌号为:%d%d%d%d\n",i,i,j,j);
                        flag=1;
                        break;
                    }
             }
         }
     }
    return 0;

}
