#include <iostream>
#define SIZE 9
using namespace std;

int parvalue[SIZE]={10000,5000,1000,500,200,100,50,20,10};
int num[SIZE]={0};
void exchange(int n)
{
    int i;
    for(i=0;i<SIZE;i++)
        if(n>=parvalue[i])break;
    while(n>0 && i<SIZE)
    {
        if(n>=parvalue[i])
        {
            n-=parvalue[i];
            num[i]++;
        }
        else if(n<10&&n>=5)
        {
            num[SIZE-1]++;
            break;
        }
        else
            i++;
    }
}
int main()
{
    double n;
    cout<<"请输入找零金额:"<<endl;
    cin>>n;
    exchange((int)(n*100.0));
    cout<<n<<"元的零钱组成:"<<endl;
    for(int i=0;i<SIZE;i++)
    {
        if(num[i]>0)
        {
            cout<<(float)parvalue[i]/100.0<<"元: "<<num[i]<<"张"<<endl;
        }
    }

    return 0;


}
