#include <iostream>
using namespace std;

unsigned int c_in_str(const char *str,char ch)
{
    unsigned int count=0;
    while(*str)       //当str指向的不为'\0'时，就进行循环
    {
        if(*str==ch)
            count++;
        str++;
    }
    return count;
}
int main()
{
    char mmm[15]="minmum";

    unsigned int ms=c_in_str(mmm,'m');
    unsigned int us=c_in_str(mmm,'n');
    cout<<ms<<" m characters in "<<mmm<<endl;
    cout<<us<<" n characters in "<<mmm<<endl;

    return 0;
}
