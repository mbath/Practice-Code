#include <stdio.h>
int main()
{
    //法一：
   /* int a,b,c,i=0;
    for(a=1;a<=5;a++)
        for(b=1;b<=5;b++)
            for(c=1;c<=5;c++)
            {
                if(a!=b&&b!=c&&c!=a)
                {
                    printf("A:%2d B:%2d C:%2d      ",a,b,c);
                    i++;
                    if(i%4==0)printf("\n");
                }
            }
            printf("一共有%d种方法\n",i);*/

    //法二：这样虽然没有减少循环的次数，但减少了运行的时间，提高了效率
    int a,b,c,i=0;
    for(a=1;a<=5;a++)
        for(b=1;b<=5;b++)
            for(c=1;c<=5&&a!=b;c++)
            {
                if(a!=c&&b!=c)
                {
                    printf("A:%2d B:%2d C:%2d      ",a,b,c);
                    i++;
                    if(i%4==0)printf("\n");
                }
            }
            printf("一共有%d种方法\n",i);

    return 0;
}
