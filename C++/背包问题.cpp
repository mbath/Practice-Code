#include <iostream>
#include <algorithm>
using namespace std;
const int M=100005;
struct three{
    double w;//每种宝物的重量
    double v;//每种宝物的价值
    double p;//每种宝物的性价比（价值/重量)
    }s[M] ;
bool cmp(three a,three b)
{
    return a.p>b.p;
}
int main()
{
    int n;
    double m;
    cout<<"请输入宝物的数量n以及毛驴的载重能力m"<<endl;
    cin>>n>>m;
    for(int i=0;i<n;i++)
    {    cin>>s[i].w>>s[i].v;
         s[i].p=s[i].v/s[i].w;

    }
    sort(s,s+n,cmp);
    double sum=0.0; //毛驴装载古董的价值
    for(int i=0;i<n;i++)
    {
        if(m>s[i].w)
        {
            m-=s[i].w;
            sum+=s[i].v;
        }
        else
        {
            sum+=m*s[i].p; //部分装入
            break;
        }
    }
    cout<<"毛驴装载的最大价值Maximum="<<sum<<endl;

    return 0;
}
