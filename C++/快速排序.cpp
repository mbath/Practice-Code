#include <iostream>
using namespace std;

int Partition(int r[],int low,int high)  //划分元素
{
    int i=low,j=high,pivot=r[low];     //基准元素
    while(i<j)
    {
        while(i<j&& r[j]>pivot)j--; //向左扫描
        while(i<j&&r[i]<pivot)i++;  //向右扫描
        if(i<j)
        {
            swap(r[i++],r[j--]);     //r[i]和r[j]后移后j左移一位
        }
    }
    if(r[i]>pivot)
    {
        swap(r[i-1],r[low]);
        return i-1;
    }
    swap(r[i],r[low]);
        return i;
}
void QuickSort(int R[],int low,int high)
{
    int mid;
    if(low<high)
    {
        mid=Partition(R,low,high);
        QuickSort(R,low,mid-1);
        QuickSort(R,mid+1,high);
    }
}

int main()
{
    int a[1000];
    int i,N;
    cout<<"请先输入要排序的数据的个数：";
    cin>>N;
    cout<<"请输入要排序的数据:"<<endl;
    for(i=0;i<N;i++)
        cin>>a[i];
    cout<<endl;

    QuickSort(a,0,N-1);
    cout<<"排序后的序列为："<<endl;
    for(i=0;i<N;i++)
    {
        cout<<a[i]<<" ";
    }
    cout<<endl;

    return 0;

}
